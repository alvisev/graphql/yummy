# README 

First steps with [`GraphQL`](https://graphql.org/) and 
[`strawberry`](https://strawberry.rocks/) :strawberry: !

This demo project is basically a recipe search system to help find and cook 
delicious recipes! It works by sending queries to a `strawberry` GraphQL 
server that retrieves data from (normally, what should be databases) REST 
endpoints somewhere on the internet 
([`TheMealDB`](https://www.themealdb.com/),
[`tacofancy`](https://github.com/evz/tacofancy-api), and 
[`recipepuppy`](http://www.recipepuppy.com/)) 
which have nice open APIs we can query.

## Usage

I am using [`poetry`](https://python-poetry.org/) to manage my project and dependencies. 

To serve this app follow these simple steps:
```bash
git clone https://gitlab.com/alvisev/graphql/yummy.git
cd yummy
poetry install
poetry run strawberry server yummy.schema
```

Then visit the url served by your `strawberry` server:
```bash
Running strawberry on http://0.0.0.0:8000/ 🍓
```

and get cooking! 🍝

## Examples:

We can look for coffee and strawberry Tiramisù! 🍰

![Tiramisu Query](extras/imgs/tiramisu.png)

What about a nice curry dish? 🍛 

![Salmon Recipe](extras/imgs/curry.png)

Try it out and see if you can get a random taco! 🌮

#### References:
- https://graphql.org/learn/
- https://strawberry.rocks/
- project inspired by https://github.com/TejasQ/basically-fullstack-graphql