import httpx
import strawberry
from enum import Enum
from typing import Optional, Dict, List, Union


# -------------
# TYPES
# -------------

@strawberry.type
class Meal:

    id: str
    name: str
    drink: Optional[str]
    category: str
    area: str
    instructions: str
    thumbnail: str
    tags: List[str]
    youtube: str
    ingredients: Optional[List[str]]
    measures: Optional[List[str]]

@strawberry.type
class Meals:

    meals: List[Meal]

@strawberry.type
class MealCategory:

    id: str
    name: str
    thumbnail: str
    description: str

@strawberry.type
class MealCategories:

    categories: List[MealCategory]

@strawberry.type
class Ingredient:

    id: str
    name: str
    description: Optional[str]
    type: Optional[str]

@strawberry.type
class Category:

    name: str

@strawberry.type
class Area:

    name: str

ListResult = Union[Ingredient, Category, Area]

@strawberry.type
class ListResults:

    results: List[ListResult]

@strawberry.type
class FilterResult:

    id: str
    name: str
    thumbnail: str

    def build_meal_from_id(self) -> Meal:
        """Build a Meal from its id"""
        return resolve_lookup(id=int(self.id))

    meal: Meal = strawberry.field(resolver=build_meal_from_id)


@strawberry.type
class FilterResults:

    results: List[FilterResult]

@strawberry.enum
class SearchTypes(Enum):
    AREA = "a"
    CATEGORY = "c"
    INGREDIENT = "i"

# -------------
# UTILS
# -------------
def build_meal(payload: Dict[str, str]) -> Meal:

    id = payload.get('idMeal')
    name = payload.get('strMeal')
    drink = payload.get('strDrinkAlternate')
    category = payload.get('strCategory')
    area = payload.get('strArea')
    instructions = payload.get('strInstructions')
    thumbnail = payload.get('strMealThumb')
    tags = payload.get('strTags')
    tags = tags.split(',') if tags else tags
    youtube = payload.get('strYoutube')
    ingredients = [payload.get(f'strIngredient{i}') for i in range(1, 21)]
    measures = [payload.get(f'strMeasure{i}') for i in range(1, 21)]


    return Meal(id=id, name=name, drink=drink, category=category, area=area,
                instructions=instructions, thumbnail=thumbnail, tags=tags,
                youtube=youtube, ingredients=ingredients, measures=measures)


def build_meal_category(payload: Dict[str, str]) -> MealCategory:

    id = payload.get('idCategory')
    name = payload.get('strCategory')
    thumbnail = payload.get('strCategoryThumb')
    description = payload.get('strCategoryDescription')

    return MealCategory(id=id, name=name, thumbnail=thumbnail,
                        description=description)


def build_area(payload: Dict[str, str]) -> Area:

    name = payload.get('strArea')

    return Area(name=name)


def build_category(payload: Dict[str, str]) -> Category:

    name = payload.get('strCategory')

    return Category(name=name)


def build_ingredient(payload: Dict[str, str]) -> Ingredient:

    id = payload.get('idIngredient')
    name = payload.get('strIngredient')
    description = payload.get('strDescription')
    type = payload.get('strType')

    return Ingredient(id=id, name=name, description=description, type=type)


def build_filter_result(payload: Dict[str, str]) -> FilterResult:

    id = payload.get('idMeal')
    name = payload.get('strMeal')
    thumbnail = payload.get('strMealThumb')

    return FilterResult(id=id, name=name, thumbnail=thumbnail)


def get_all(token: SearchTypes) -> List[str]:
    """List all available database Areas, Categories or Ingredients"""

    params = {token.value: 'list'}
    r = httpx.get('https://www.themealdb.com/api/json/v1/1/list.php',
                  params=params)

    response = r.json()
    payloads = response['meals']

    if token == SearchTypes.AREA:
        name = 'strArea'
    elif token == SearchTypes.CATEGORY:
        name = 'strCategory'
    else:
        name = 'strIngredient'

    return [payload.get(name) for payload in payloads]


# -------------
# RESOLVERS
# -------------
def resolve_search_mealdb(query: str) -> Meals:

    params = {'f': query} if len(query) == 1 else {'s': query}

    r = httpx.get('https://www.themealdb.com/api/json/v1/1/search.php',
                  params=params)

    response = r.json()
    payloads = response['meals']

    meals = [build_meal(payload) for payload in payloads] if payloads else []

    return Meals(meals=meals)

def resolve_lookup(id: int) -> Meal:

    params = {'i': id}
    r = httpx.get('https://www.themealdb.com/api/json/v1/1/lookup.php',
                  params=params)

    response = r.json()
    payload = response['meals'][0]

    meal = build_meal(payload)
    return meal

def resolve_random() -> Meal:

    r = httpx.get('https://www.themealdb.com/api/json/v1/1/random.php')
    response = r.json()
    payload = response['meals'][0]

    meal = build_meal(payload)
    return meal

def resolve_categories() -> MealCategories:

    r = httpx.get('https://www.themealdb.com/api/json/v1/1/categories.php')
    response = r.json()
    payloads = response['categories']

    meal_categories = [build_meal_category(payload) for payload in payloads]
    return MealCategories(categories=meal_categories)

def resolve_list(by: SearchTypes) -> ListResults:

    params = {by.value: 'list'}
    r = httpx.get('https://www.themealdb.com/api/json/v1/1/list.php',
                  params=params)

    response = r.json()
    payloads = response['meals']

    if by == SearchTypes.AREA:
        f = build_area
    elif by == SearchTypes.CATEGORY:
        f = build_category
    else:
        f = build_ingredient

    results = [f(payload) for payload in payloads]
    return ListResults(results=results)


def resolve_filter(by: str) -> FilterResults:

    # Check if we are filtering by Area, Category, or Ingredient
    if by.title() in get_all(SearchTypes.AREA):
        token = SearchTypes.AREA.value
    elif by.title() in get_all(SearchTypes.CATEGORY):
        token = SearchTypes.CATEGORY.value
    else:
        token = 'i'

    params = {token: by}
    r = httpx.get('https://www.themealdb.com/api/json/v1/1/filter.php',
                  params=params)

    response = r.json()
    payloads = response['meals']

    results = [build_filter_result(payload) for payload in payloads]
    return FilterResults(results=results)

# -------------
# QUERIES
# -------------
@strawberry.type
class Query:

    @strawberry.field
    def search_mealdb(self, query: str) -> Meals:
        return resolve_search_mealdb(query=query)

    @strawberry.field
    def lookup(self, id: int) -> Meal:
        return resolve_lookup(id=id)

    @strawberry.field
    def random(self) -> Meal:
        return resolve_random()

    @strawberry.field
    def categories(self) -> MealCategories:
        return resolve_categories()

    @strawberry.field
    def list(self, by: SearchTypes) -> ListResults:
        return resolve_list(by=by)

    @strawberry.field
    def filter(self, by: str) -> FilterResults:
        return resolve_filter(by=by)