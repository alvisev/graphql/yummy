import httpx
import strawberry
from typing import Optional, Dict, List, Union

from mealdb.schema import Meal, Meals

# -------------
# TYPES
# -------------

# -------------
# UTILS
# -------------
def build_meal(payload: Dict[str, str]) -> Meal:

    name = payload.get('title')
    instructions = payload.get('href')
    thumbnail = payload.get('thumbnail')
    ingredients = payload.get('ingredients')
    ingredients = [i for i in ingredients.split(',')] if ingredients else ingredients

    return Meal(id=None, name=name, drink=None, category=None, area=None,
                instructions=instructions, thumbnail=thumbnail, tags=None,
                youtube=None, ingredients=ingredients, measures=None)

# -------------
# RESOLVERS
# -------------
def resolve_search_puppy(query: str, ingredients: str) -> Meals:

    params = {'q': query, 'i': ingredients}
    r = httpx.get('http://www.recipepuppy.com/api/', params=params)
    response = r.json()

    payloads = response.get('results')

    meals = [build_meal(payload) for payload in payloads]
    return Meals(meals=meals)

# -------------
# QUERIES
# -------------
@strawberry.type
class Query:

    @strawberry.field
    def search_puppy(self, query: str = None, ingredients: str = None) -> Meals:
        return resolve_search_puppy(query=query, ingredients=ingredients)
