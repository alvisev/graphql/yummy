import httpx
import strawberry
from typing import Optional, Dict, List, Union


# -------------
# TYPES
# -------------
@strawberry.type
class TacoField:

    url: str
    recipe: str
    slug: str
    name: str


@strawberry.type
class Taco:

    seasoning: TacoField
    condiment: TacoField
    mixin: TacoField
    baseLayer: TacoField
    shell: TacoField


# -------------
# UTILS
# -------------
def build_taco_field(payload: Dict[str, str]) -> TacoField:

    url = payload.get('url')
    recipe = payload.get('recipe')
    slug = payload.get('slug')
    name = payload.get('name')

    return TacoField(url=url, recipe=recipe, slug=slug, name=name)

# -------------
# RESOLVERS
# -------------
def resolve_taco() -> Taco:


    r = httpx.get('https://taco-randomizer.herokuapp.com/random/')
    response = r.json()

    seasoning_payload = response.get('seasoning')
    condiment_payload = response.get('condiment')
    mixin_payload = response.get('mixin')
    base_layer_payload = response.get('base_layer')
    shell_payload = response.get('shell')

    seasoning = build_taco_field(seasoning_payload)
    condiment = build_taco_field(condiment_payload)
    mixin = build_taco_field(mixin_payload)
    base_layer = build_taco_field(base_layer_payload)
    shell = build_taco_field(shell_payload)

    return Taco(seasoning=seasoning, condiment=condiment, mixin=mixin,
                baseLayer=base_layer, shell=shell)

# -------------
# QUERIES
# -------------
@strawberry.type
class Query:

    @strawberry.field
    def taco(self) -> Taco:
        return resolve_taco()
