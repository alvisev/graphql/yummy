import strawberry
from enum import Enum
from typing import Optional, Dict, List, Union

import mealdb.schema
import tacodb.schema
import recipepuppydb.schema

# -------------
# TYPES
# -------------



# -------------
# UTILS
# -------------


# -------------
# RESOLVERS
# -------------
def resolve_search(query: str, ingredients: str) -> mealdb.schema.Meals:
    """Search for Meals in both databases"""

    meals_puppydb = recipepuppydb.schema.resolve_search_puppy(
        query=query, ingredients=ingredients).meals

    if query and not ingredients:
        meals_mealdb = mealdb.schema.resolve_search_mealdb(query=query).meals
    else:
        meals_mealdb = []

    return mealdb.schema.Meals(meals=meals_mealdb + meals_puppydb)


# -------------
# QUERIES
# -------------
@strawberry.type
class Query(mealdb.schema.Query,
            tacodb.schema.Query,
            recipepuppydb.schema.Query):

    # Collect all queries from specialised endpoint schemas

    @strawberry.field
    def search(self, query: str = None,
               ingredients: str = None) -> mealdb.schema.Meals:
        return resolve_search(query=query, ingredients=ingredients)

# -------------
# SCHEMA
# -------------
schema = strawberry.Schema(query=Query)

# Serve this schema with:
# poetry run strawberry server yummy.schema